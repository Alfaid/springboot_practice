package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CourseDao;
import com.model.Course;
import com.model.Student;

@RestController
public class CourseController {

	@Autowired
	CourseDao courseDao;
	
	@GetMapping("getCourses")
	public List<Course> getCourses() {
		return courseDao.getCourses();
	}
	
	@GetMapping("getCourseById/{id}")
	public Course getCourseById(@PathVariable("id") int CId) {
		return courseDao.getCourseById(CId);
	}
	
	@GetMapping("getCourseByName/{name}")
	public Course getCourseByName(@PathVariable("name") String CName) {
		return courseDao.getCourseByName(CName);
	}
	
	@PostMapping("addCourse")
	public Course addCourse(@RequestBody Course course){
		return courseDao.addStudent(course);
	}
	@PutMapping("updateCourse")
	public  Course updateCourse(@RequestBody Course course){
		return courseDao.updateStudent(course);
	}
	
	@DeleteMapping("deleteCourseById/{id}")
	public String deleteStudentById(@PathVariable("id") int CId){
		courseDao.deleteStudentById(CId);
		return "Employee with EmployeeId: " +CId+ ", Deleted Successfully";
	}
}
