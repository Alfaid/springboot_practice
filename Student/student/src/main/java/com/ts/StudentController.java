package com.ts;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.StudentDAO;
import com.model.Student;


@RestController
public class StudentController {
	@Autowired
	StudentDAO studDAO;
	
	@GetMapping("getStudents")
	public List<Student> getStudents() {
		return studDAO.getStudents();
	}
	
	@GetMapping("getStudentById/{id}")
	public Student getStudentById(@PathVariable("id") int stdId) {
		return studDAO.getStudentById(stdId);
	}
	
	@GetMapping("getStudentByName/{name}")
	public Student getStudentByName(@PathVariable("name") String stdName) {
		return studDAO.getStudentByName(stdName);
	}
	
	@PostMapping("addStudent")
	public Student addStudent(@RequestBody Student student){
		return studDAO.addStudent(student);
	}
	@PutMapping("updateStudent")
	public  Student updateStudent(@RequestBody Student student){
		return studDAO.updateStudent(student);
	}
	
	@DeleteMapping("deleteStudentById/{id}")
	public String deleteStudentById(@PathVariable("id") int stdId){
		studDAO.deleteStudentById(stdId);
		return "Employee with EmployeeId: " +stdId+ ", Deleted Successfully";
	}
	
	@GetMapping("getLogin/{email}/{password}")
	public Student getLogin(@PathVariable("email") String emailId,@PathVariable("password") String password) {
		return studDAO.getLogin(emailId,password);
	}


	


	
	/*
	@GetMapping("getStudent")
	public Student getStudent(){
		Student student = new Student();
		student.setStudentId(101);
		student.setStudentName("Alfaid");
		student.setFees(45000.00);
		return student;
	}
	
	@GetMapping("getAllStudents")
	public List<Student> getAllStudents(){
		
		List<Student> studentList = new ArrayList<Student>();
		Student student1 = new Student(101, "Alfaid", 45000.00);
		Student student2 = new Student(102, "Pranay", 45000.00);
		Student student3 = new Student(103, "Sresta", 45000.00);
		Student student4 = new Student(104, "lakshmi", 45000.00);
		Student student5 = new Student(105, "karunya", 45000.00);
		
		studentList.add(student1);
		studentList.add(student2);
		studentList.add(student3);
		studentList.add(student4);
		studentList.add(student5);
		
		return studentList;
	}*/

	

}
