package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ManyToAny;

@Entity
public class Student {
	
	@Id@GeneratedValue
	private int studentId;
	private String studentName;
	private int age;
	private double percentage;
	private String emailId;
	private String password;
	
	@ManyToOne
	@JoinColumn(name = "courseId")
	Course course;

	public Student() {
		super();
		}

	public Student(int studentId, String studentName, int age, double percentage, String emailId, String password,
			Course course) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.age = age;
		this.percentage = percentage;
		this.emailId = emailId;
		this.password = password;
		this.course = course;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", studentName=" + studentName + ", age=" + age + ", percentage="
				+ percentage + ", emailId=" + emailId + ", password=" + password + ", course=" + course + "]";
	}
	
	
	
	
}